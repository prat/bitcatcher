//
// Created by riri on 8/08/15.
//

#include <opencv2/opencv.hpp>
#include <pthread.h>
#include "receiver.h"

using namespace cv;
Server server;
pthread_t server_thread;

bool exit_time = 0;

void* runServer(void *arg) {
    std::string port = "5555";
    initalize_server(server, port.c_str());
    while(!exit_time) receive_frames(server);
    terminate_server(server);
    return NULL;
}

int main(int argv, char* argc[]) {
    Mat A(360, 288, CV_8UC3);
    namedWindow("test window", WINDOW_AUTOSIZE);
    server = new_server(0);
    if (pthread_create(&server_thread, NULL, &runServer, NULL)) {
        assert(false && "Thread failed to start");
    }
    first_frame(server);
    int k = -1;
    while(k == -1) {
        imshow("test window", A);
        k = waitKey(1);
        frame_pause(server);
        for (int vane = 0; vane < 360; vane++) {
            for (int mag = 0; mag < 288; mag++) {
                int pixel_pos = vane * 288 * 3 + mag * 3;
                unsigned char r = server->output_frame[pixel_pos + 0];
                unsigned char g = server->output_frame[pixel_pos + 1];
                unsigned char b = server->output_frame[pixel_pos + 2];

                A.at<char>(pixel_pos+0) = b;
                A.at<char>(pixel_pos+1) = g;
                A.at<char>(pixel_pos+2) = r;
            }
        }
    }
    last_frame(server);
    exit_time = 1;
    void *result = NULL;
    pthread_join(server_thread, &result);
    free_server(server);
    A.release();
    return 0;
}