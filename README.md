### Instructions ###

Create BitCatcher folder, e.g. /home/<user>/BitCatcher/

Do this:


```
> mkdir /home/<user>/BitCatcher
> cd /home/<user>/BitCatcher
> git clone https://prat@bitbucket.org/prat/bitcatcher.git
> mkdir build
> cd build
> cmake ../src
> make
> sudo make install
```